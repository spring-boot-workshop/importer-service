package me.bleidner.spring.rest;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class EndpointTest {

    @LocalServerPort
    private int port;

    @Test
    void shouldAcceptPostRequest() {

        // TODO: Implement this test with Rest Assured
    }
}
