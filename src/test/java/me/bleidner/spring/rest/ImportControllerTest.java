package me.bleidner.spring.rest;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import me.bleidner.spring.model.Product;
import me.bleidner.spring.service.ImportService;


@WebMvcTest
public class ImportControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private ImportService service;

    @Test
    void shouldHaveImportEndpoint() throws Exception {

        mvc.perform(post("/imports/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[]"))
                .andExpect(status().isOk());

    }

    @Test
    void shouldPassProductsToService() throws Exception {

        List<Product> products = List.of(new Product("iPhone", 399.0));
        String json = mapper.writeValueAsString(products);

        mvc.perform(post("/imports/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isOk());

        verify(service).importProducts(eq(products));
    }

    @Test
    void shouldAllowAdminForDeleteEndpoint() throws Exception {

        mvc.perform(delete("/imports/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(""))
                .andExpect(status().isOk());
    }
}
