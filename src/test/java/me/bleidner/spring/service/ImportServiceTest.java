package me.bleidner.spring.service;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import me.bleidner.spring.messaging.MessageSender;
import me.bleidner.spring.model.Product;


public class ImportServiceTest {

    private ImportService importService;

    private MessageSender sender;

    @BeforeEach
    void setUp() {

        sender = mock(MessageSender.class);
        importService = new ImportService(sender);
    }

    @Test
    void shouldSendMessageForEachProduct() {

        Product iPhone = new Product("iPhone", 999.0);
        importService.importProducts(List.of(iPhone, iPhone));

        verify(sender, times(2)).send(eq(iPhone));
    }
}
