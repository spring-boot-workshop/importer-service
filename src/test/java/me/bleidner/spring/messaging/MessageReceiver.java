package me.bleidner.spring.messaging;

import java.util.concurrent.CountDownLatch;

import org.springframework.amqp.rabbit.annotation.RabbitListener;

import me.bleidner.spring.model.Product;


public class MessageReceiver {

    private Product receivedProduct;
    private final CountDownLatch countDownLatch;

    public Product getReceivedProduct() {

        return receivedProduct;
    }

    public CountDownLatch getCountDownLatch() {

        return countDownLatch;
    }

    public MessageReceiver() {
        countDownLatch = new CountDownLatch(1);
    }

    @RabbitListener(queues = "products")
    public void listen(Product product) {
        countDownLatch.countDown();
        receivedProduct = product;
    }
}
