package me.bleidner.spring.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import me.bleidner.spring.model.Product;
import me.bleidner.spring.service.ImportService;


@RestController
public class ImportController {

    @Autowired
    private ImportService service;

    @PostMapping("/imports/products")
    public void importProducts(@RequestBody List<Product> products) {

        service.importProducts(products);
    }

    @DeleteMapping("/imports/products")
    public String deleteProducts() {
        return "DELETED";
    }

}
