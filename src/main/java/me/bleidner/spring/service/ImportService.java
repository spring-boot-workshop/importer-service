package me.bleidner.spring.service;

import java.util.List;

import lombok.AllArgsConstructor;
import me.bleidner.spring.messaging.MessageSender;
import me.bleidner.spring.model.Product;


@AllArgsConstructor
public class ImportService {

    private MessageSender sender;

    public void importProducts(List<Product> products) {
        products.forEach(product -> sender.send(product));
    }
}
