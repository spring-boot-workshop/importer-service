package me.bleidner.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@EnableScheduling
public class ProductImporterServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProductImporterServiceApplication.class, args);
    }

}
