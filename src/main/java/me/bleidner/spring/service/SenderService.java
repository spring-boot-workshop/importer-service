package me.bleidner.spring.service;

import org.springframework.stereotype.Component;

import lombok.extern.log4j.Log4j2;


@Component
@Log4j2
public class SenderService {

    //    private final Faker faker = new Faker();
    //
    //    @Autowired
    //    private MessageSender sender;
    //
    //    @Scheduled(fixedRate = 5000)
    //    public void reportCurrentTime() {
    //
    //        Product product = new Product(faker.commerce().productName(), Math.random());
    //        log.info(product.toString());
    //        sender.send(product);
    //    }
}
